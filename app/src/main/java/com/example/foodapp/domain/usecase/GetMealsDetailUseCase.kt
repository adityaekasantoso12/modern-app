package com.example.foodapp.domain.usecase

import com.example.foodapp.domain.model.Meals
import com.example.foodapp.domain.repository.MealsRepository
import kotlinx.coroutines.flow.Flow

class GetMealsDetailUseCase (
    private val mealsRepository: MealsRepository
){
    operator fun invoke(id: String): Flow<Meals?> {
        return mealsRepository.getMealsDetail(
            id = id
        )
    }

}