package com.example.foodapp.domain.usecase

import com.example.foodapp.domain.model.Meals
import com.example.foodapp.domain.repository.MealsRepository
import kotlinx.coroutines.flow.Flow

class GetMealsByCategoryUseCase(
    private val mealsRepository: MealsRepository
) {
    operator fun invoke(category: String): Flow<List<Meals>> {
        return mealsRepository.getMealsByCategory(
            category = category
        )
    }
}
