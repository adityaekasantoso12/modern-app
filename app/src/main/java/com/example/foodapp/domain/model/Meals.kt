package com.example.foodapp.domain.model

data class Meals(
    val id: String,
    val name: String,
    val image: String
)
