package com.example.foodapp.domain.repository

import com.example.foodapp.domain.model.Meals
import kotlinx.coroutines.flow.Flow

interface MealsRepository {
    fun getMealsByCategory(category: String): Flow<List<Meals>>
    fun getMealsDetail(id: String): Flow<Meals?>
}