package com.example.foodapp.di

import com.example.foodapp.data.source.MealsDataSource
import com.example.foodapp.data.source.MealsDataSourceImpl
import org.koin.dsl.module

val dataSourceModule = module {
    single<MealsDataSource> {
        MealsDataSourceImpl(get())
    }
}