package com.example.foodapp.di

import com.example.foodapp.domain.usecase.GetMealsByCategoryUseCase
import com.example.foodapp.domain.usecase.GetMealsDetailUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory {
        GetMealsByCategoryUseCase(get())
    }
    factory {
        GetMealsDetailUseCase(get())
    }

}