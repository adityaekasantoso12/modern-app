package com.example.foodapp.di

import com.example.foodapp.ui.feature.DetailScreenViewModel
import com.example.foodapp.ui.feature.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        MainViewModel(get())
    }
    viewModel {
        DetailScreenViewModel(get())
    }
}