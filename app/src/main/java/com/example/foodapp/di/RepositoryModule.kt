package com.example.foodapp.di

import com.example.foodapp.data.repository.MealsRepositoryImpl
import com.example.foodapp.domain.repository.MealsRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val repositoryModule = module {
    single<MealsRepository> {
        MealsRepositoryImpl(get(),get(named(MealsDispatchers.IO)))
    }
}