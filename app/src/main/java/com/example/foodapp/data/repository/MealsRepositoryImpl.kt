package com.example.foodapp.data.repository

import com.example.foodapp.data.repository.mapping.mappingToDetailUseCaseEntity
import com.example.foodapp.data.repository.mapping.mappingToUseCaseEntity
import com.example.foodapp.data.source.MealsDataSource
import com.example.foodapp.domain.model.Meals
import com.example.foodapp.domain.repository.MealsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class MealsRepositoryImpl(
    private val mealsDataSource: MealsDataSource,
    private val dispatcher: CoroutineDispatcher,
): MealsRepository {
    override fun getMealsByCategory(category: String): Flow<List<Meals>> {
        return flow {
            emit(
                mealsDataSource.getMealsByCategory(
                    category = category
                ).meals.mappingToUseCaseEntity()
            )
        }.flowOn(dispatcher)

    }
    override fun getMealsDetail(id: String): Flow<Meals?> {
        return flow {
            emit(
                mealsDataSource.getMealsDetail(
                    id = id
                ).meals.mappingToDetailUseCaseEntity()
            )
        }.flowOn(dispatcher)
    }
}