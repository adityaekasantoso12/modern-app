package com.example.foodapp.data.source.service

import com.example.foodapp.data.source.response.MealsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MealsService {
    @GET("filter.php")
    suspend fun getMealsByCategory(
        @Query("c") category: String
    ): MealsResponse

    @GET("lookup.php")
    suspend fun  getMealsDetail(
        @Query("i") id: String
    ): MealsResponse
}