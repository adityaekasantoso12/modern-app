package com.example.foodapp.data.source

import com.example.foodapp.data.source.response.MealsResponse
import com.example.foodapp.data.source.service.MealsService

class MealsDataSourceImpl(
    private val mealsService: MealsService
): MealsDataSource {
    override suspend fun getMealsByCategory(category: String): MealsResponse {
        return mealsService.getMealsByCategory(
            category = category
        )
    }
    override suspend fun getMealsDetail(id: String): MealsResponse {
        return mealsService.getMealsDetail(
            id = id
        )
    }
}