package com.example.foodapp.data.repository.mapping

import com.example.foodapp.data.source.response.Meal
import com.example.foodapp.domain.model.Meals


fun List<Meal>?.mappingToUseCaseEntity(): List<Meals> {
    val newList: MutableList<Meals> = mutableListOf()

    this?.forEach {
        newList.add(
            Meals(
                id = it.idMeal,
                name = it.strMeal,
                image = it.strMealThumb
            )
        )
    }
    return if (this.isNullOrEmpty()) {
        emptyList()
    } else {
        newList
    }
}
fun List<Meal>?.mappingToDetailUseCaseEntity(): Meals? {
    return if (this.isNullOrEmpty()){
        null
    }else{
        this?.get(0)?.let {
            Meals(
                id = it.idMeal,
                name = it.strMeal,
                image = it.strMealThumb
            )
        }
    }
}