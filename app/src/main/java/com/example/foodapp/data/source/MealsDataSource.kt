package com.example.foodapp.data.source

import com.example.foodapp.data.source.response.MealsResponse

interface MealsDataSource {
    suspend fun getMealsByCategory(category: String): MealsResponse
    suspend fun getMealsDetail(id: String): MealsResponse
}