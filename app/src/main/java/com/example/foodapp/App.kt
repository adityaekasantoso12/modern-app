package com.example.foodapp

import android.app.Application
import com.example.foodapp.di.dataSourceModule
import com.example.foodapp.di.dispatcherModule
import com.example.foodapp.di.networkModule
import com.example.foodapp.di.repositoryModule
import com.example.foodapp.di.useCaseModule
import com.example.foodapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                listOf(
                    dataSourceModule,
                    dispatcherModule,
                    networkModule,
                    repositoryModule,
                    useCaseModule,
                    viewModelModule
                )
            )
        }
    }
}