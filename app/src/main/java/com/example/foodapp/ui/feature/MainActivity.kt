package com.example.foodapp.ui.feature

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavArgument
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import com.example.foodapp.domain.model.Meals
import com.example.foodapp.ui.feature.model.MainEventState
import com.example.foodapp.ui.feature.model.MainUiState
import com.example.foodapp.ui.theme.FoodAppTheme
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            FoodAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavHost(navController = navController, startDestination = "main") {
                        composable(route = "main") {
                            MainRoute(navController = navController)
                        }
                        composable(route = "detail/{mealId}",
                            arguments = listOf(navArgument("mealId"){type = NavType.StringType}
                        ))
                        {
                            val id = it.arguments?.getString("mealId")?:"0"
                            DetailRoute(mealId = id, navController = navController)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun MainRoute(
    viewModel: MainViewModel = koinViewModel(),
    navController: NavController

) {
    val uiState = viewModel.uiState.collectAsState()

    MainScreen(
        uiState = uiState.value,
        value = viewModel.search,
        onValueChange = {
            viewModel.onEvent(
                event = MainEventState.SearchOnChange(it)
            )
        },
        onClickSearch = {
            viewModel.onEvent(
                event = MainEventState.Search
            )
        },
        onClickItem = { meal ->
            navController.navigate("detail/${meal.id}")
        }
    )
}

@Composable
fun MainScreen(
    uiState: MainUiState,
    value: String,
    onValueChange: (String) -> Unit,
    onClickSearch: () -> Unit,
    onClickItem: (Meals) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(
                all = 16.dp
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Search(
            value = value,
            onValueChange = onValueChange,
            onClick = onClickSearch
        )

        Spacer(
            modifier = Modifier
                .padding(
                    top = 16.dp
                )
        )

        when(uiState) {
            MainUiState.Loading -> {
                CircularProgressIndicator()
            }

            is MainUiState.Success -> {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    content = {
                        items(
                            uiState.data,
                            key = {
                                it.id
                            }
                        ) { meal ->
                            ItemList(
                                id = meal.id,
                                name = meal.name,
                                image = meal.image,
                                onClick = {
                                     onClickItem.invoke(meal)
                                }
                            )
                        }
                    }
                )
            }

            is MainUiState.Error -> {
                Text(text = uiState.error)
            }

            MainUiState.Empty -> {
                Text(text = "Data Kosong Ni")
            }

            MainUiState.Idle -> {
                Text(text = "Ayok Lakukan Pencarian")
            }

        }

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Search(
    value: String,
    onValueChange: (String) -> Unit,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
    ) {
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            placeholder = {
                Text(text = "Search")
            }
        )

        Spacer(
            modifier = Modifier
                .padding(
                    start = 16.dp
                )
        )

        Button(onClick = onClick) {
            Text(text = "Search")
        }
    }
}

@Composable
fun ItemList(
    id: String,
    name: String,
    image: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .clickable {
                onClick.invoke()
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            model = image,
            contentDescription = name,
            modifier = Modifier
                .size(100.dp)
                .clip(RoundedCornerShape(16.dp))
        )

        Spacer(
            modifier = Modifier
                .padding(
                    start = 16.dp
                )
        )

        Text(
            text = name,
            modifier = Modifier
                .fillMaxWidth()
        )
    }
}
