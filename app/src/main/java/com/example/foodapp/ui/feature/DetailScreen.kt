package com.example.foodapp.ui.feature

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.example.foodapp.ui.feature.model.DetailUiState
import org.koin.androidx.compose.koinViewModel

@Composable
fun DetailScreen(
    id: String,
    name: String,
    image: String
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        item {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .background(Color.Gray)
            ) {
                Image(
                    painter = rememberAsyncImagePainter(image),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
        item {
            Spacer(modifier = Modifier.height(16.dp))

            Text(
                text = name,
                style = MaterialTheme.typography.bodyLarge.copy(
                    fontWeight = FontWeight.SemiBold
                ),
            )
        }
    }
}

@Composable
fun DetailRoute(
    mealId: String,
    viewModel: DetailScreenViewModel = koinViewModel(),
    navController: NavController
){
    val uiState = viewModel.uiState.collectAsState()
    val uiStateDetail:DetailUiState = uiState.value

    LaunchedEffect(key1 = mealId, block ={
        viewModel.getMealDetailById(mealId)
    } )

    when (uiStateDetail){
       is DetailUiState.Success ->{
           val data = uiStateDetail.data
           DetailScreen(
               id = data.id,
               name = data.name,
               image = data.image
           )
        }

        DetailUiState.Empty -> Text(text = "Data tidak ditemukan")
        is DetailUiState.Error -> {
            var message = uiStateDetail.error
            Text(text = message)
        }
        DetailUiState.Idle -> Text(text = "Data tidak diteukan")
        DetailUiState.Loading -> CircularProgressIndicator()
    }

}

@Preview(showBackground = true)
@Composable
fun DetailScreenPreview() {
    DetailScreen(id = "", name = "Nama Menu", image = "")
}