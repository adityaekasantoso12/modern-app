package com.example.foodapp.ui.feature.model

import com.example.foodapp.domain.model.Meals

sealed interface MainUiState {
    object Idle: MainUiState
    object Loading: MainUiState
    object Empty: MainUiState

    data class Success(val data: List<Meals>): MainUiState
    data class Error(val error: String): MainUiState
}