package com.example.foodapp.ui.feature

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodapp.domain.usecase.GetMealsByCategoryUseCase
import com.example.foodapp.ui.feature.model.MainEventState
import com.example.foodapp.ui.feature.model.MainUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MainViewModel(
    private val getMealsByCategoryUseCase: GetMealsByCategoryUseCase
) : ViewModel() {
    var search by mutableStateOf("")
        private set

    private val _uiState: MutableStateFlow<MainUiState> = MutableStateFlow(MainUiState.Idle)
    val uiState get() = _uiState.asStateFlow()

    fun onEvent(event: MainEventState) {
        when(event) {
            is MainEventState.SearchOnChange -> {
                search = event.search
            }

            MainEventState.Search -> {
                getMealsByCategory()
            }
        }
    }

    private fun getMealsByCategory() {
        viewModelScope.launch {
            getMealsByCategoryUseCase.invoke(
                category = search
            ).onStart {
                _uiState.update {
                    MainUiState.Loading
                }
            }.catch { throwable ->
                _uiState.update {
                    MainUiState.Error(
                        error = throwable.message ?: "Data Error"
                    )
                }
            }.collectLatest { data ->
                if (data.isEmpty()) {
                    _uiState.update {
                        MainUiState.Empty
                    }
                } else {
                    _uiState.update {
                        MainUiState.Success(
                            data = data
                        )
                    }
                }
            }
        }
    }
}