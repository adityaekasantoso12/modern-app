package com.example.foodapp.ui.feature

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodapp.domain.usecase.GetMealsByCategoryUseCase
import com.example.foodapp.domain.usecase.GetMealsDetailUseCase
import com.example.foodapp.ui.feature.model.DetailUiState
import com.example.foodapp.ui.feature.model.MainEventState
import com.example.foodapp.ui.feature.model.MainUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class DetailScreenViewModel (
    private val getMealsDetailUseCase: GetMealsDetailUseCase
) : ViewModel() {
    var search by mutableStateOf("")
        private set

    private val _uiState: MutableStateFlow<DetailUiState> = MutableStateFlow(DetailUiState.Idle)
    val uiState get() = _uiState.asStateFlow()

    fun getMealDetailById(id: String) {
        viewModelScope.launch {
            getMealsDetailUseCase.invoke(
                id = id
            ).onStart {
                _uiState.update {
                    DetailUiState.Loading
                }
            }.catch { throwable ->
                _uiState.update {
                    DetailUiState.Error(
                        error = throwable.message ?: "Data Error"
                    )
                }
            }.collectLatest { data ->
                if (data == null) {
                    _uiState.update {
                        DetailUiState.Empty
                    }
                } else {
                    _uiState.update {
                        DetailUiState.Success(
                            data = data
                        )
                    }
                }
            }
        }
    }
}