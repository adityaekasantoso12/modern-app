package com.example.foodapp.ui.feature.model

import com.example.foodapp.domain.model.Meals

sealed interface DetailUiState {
    object Idle: DetailUiState
    object Loading: DetailUiState
    object Empty: DetailUiState

    data class Success(val data: Meals): DetailUiState
    data class Error(val error: String): DetailUiState
}