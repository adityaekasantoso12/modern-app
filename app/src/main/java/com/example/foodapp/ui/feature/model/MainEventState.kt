package com.example.foodapp.ui.feature.model

sealed class MainEventState {
    data class SearchOnChange(val search: String): MainEventState()

    object Search : MainEventState()
}
